﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SVGConverter
{
    public class Vector2
    {
        public float X;
        public float Y;

        public Vector2(float _x, float _y)
        {
            X = _x;
            Y = _y;
        }

        public float Length()
        {
            return (float)Math.Sqrt(X * X + Y * Y);
        }

        public float SafeNormalize(Vector2 fallback)
        {
            float len = Length();
            if (len < 0.00001f)
            {
                X = fallback.X;
                Y = fallback.Y;
                return 0.0f;
            }
            X /= len;
            Y /= len;

            return len;
        }

        public float Dot(Vector2 v)
        {
            return X * v.X + Y * v.Y;
        }

        public static Vector2 operator+ (Vector2 p0, Vector2 p1)
        {
            return new Vector2(p0.X+p1.X, p0.Y+p1.Y);
        }

        public static Vector2 operator -(Vector2 p0, Vector2 p1)
        {
            return new Vector2(p0.X - p1.X, p0.Y - p1.Y);
        }

        public static Vector2 operator *(Vector2 p0, float s)
        {
            return new Vector2(p0.X*s, p0.Y*s);
        }

        public static Vector2 operator /(Vector2 p0, float s)
        {
            return new Vector2(p0.X / s, p0.Y / s);
        }
    }

    public interface IDrawContext
    {
        void PenUp();
        void PenDown();        
        void MoveTo(Vector2 p);
    }

    public interface ISegment
    {
        Vector2 Start { get; }
        Vector2 End { get; }
        ISegment[] Linearize();
        void Draw(IDrawContext drawer);
    }

    public class MoveToSegment : ISegment
    {
        Vector2 p0;
        public Vector2 Start { get { return p0; } }
        public Vector2 End { get { return p0; } }

        public MoveToSegment(Vector2 to)
        {
            p0 = to;
        }

        public ISegment[] Linearize()
        {
            return new ISegment[] { this };
        }

        public void Draw(IDrawContext ctx)
        {
            ctx.PenUp();
            ctx.MoveTo(p0);
            ctx.PenDown();
        }
    }

    public class LinearSegment : ISegment
    {
        Vector2 p0;
        Vector2 p1;
        public Vector2 Start { get { return p0; } }
        public Vector2 End { get { return p1; } }

        public LinearSegment(Vector2 from, Vector2 to)
        {
            p0 = from;
            p1 = to;
        }

        public ISegment[] Linearize()
        {
            return new ISegment[] { this };
        }

        public void Draw(IDrawContext ctx)
        {
            //assuming we already are in p0
            ctx.MoveTo(p1);
        }
    }

    public class BezierCurve : ISegment
    {
        public Vector2 p0, p1, p2, p3;

        public Vector2 Start { get { return p0; } }
        public Vector2 End { get { return p3; } }

        public BezierCurve(Vector2 _p0, Vector2 _p1, Vector2 _p2, Vector2 _p3)
        {
            p0 = _p0;
            p1 = _p1;
            p2 = _p2;
            p3 = _p3;
        }

        public ISegment[] Linearize()
        {
            throw new NotImplementedException();
        }

        public void Draw(IDrawContext drawer)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// This code has been copied from SVG library code from here https://github.com/vvvv/SVG
    /// </summary>
    public class SvgPathBuilder
    {
        internal class CoordinateParser
        {
            private enum NumState
            {
                invalid,
                separator,
                prefix,
                integer,
                decPlace,
                fraction,
                exponent,
                expPrefix,
                expValue
            }

            private string _coords;
            private int _pos = 0;
            private NumState _currState = NumState.separator;
            private NumState _newState = NumState.separator;
            private int i = 0;
            private bool _parseWorked = true;

            public int Position { get { return _pos; } }

            public CoordinateParser(string coords)
            {
                _coords = coords;
                if (string.IsNullOrEmpty(_coords)) _parseWorked = false;
                if (char.IsLetter(coords[0])) i++;
            }

            public bool HasMore { get { return _parseWorked; } }

            private bool MarkState(bool state)
            {
                _parseWorked = state;
                i++;
                return state;
            }

            public bool TryGetBool(out bool result)
            {
                while (i < _coords.Length && _parseWorked)
                {
                    switch (_currState)
                    {
                        case NumState.separator:
                            if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.separator;
                            }
                            else if (_coords[i] == '0')
                            {
                                result = false;
                                _newState = NumState.separator;
                                _pos = i + 1;
                                return MarkState(true);
                            }
                            else if (_coords[i] == '1')
                            {
                                result = true;
                                _newState = NumState.separator;
                                _pos = i + 1;
                                return MarkState(true);
                            }
                            else
                            {
                                result = false;
                                return MarkState(false);
                            }
                            break;
                        default:
                            result = false;
                            return MarkState(false);
                    }
                    i++;
                }
                result = false;
                return MarkState(false);
            }

            public bool TryGetFloat(out float result)
            {
                while (i < _coords.Length && _parseWorked)
                {
                    switch (_currState)
                    {
                        case NumState.separator:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.integer;
                            }
                            else if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.separator;
                            }
                            else
                            {
                                switch (_coords[i])
                                {
                                    case '.':
                                        _newState = NumState.decPlace;
                                        break;
                                    case '+':
                                    case '-':
                                        _newState = NumState.prefix;
                                        break;
                                    default:
                                        _newState = NumState.invalid;
                                        break;
                                }
                            }
                            break;
                        case NumState.prefix:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.integer;
                            }
                            else if (_coords[i] == '.')
                            {
                                _newState = NumState.decPlace;
                            }
                            else
                            {
                                _newState = NumState.invalid;
                            }
                            break;
                        case NumState.integer:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.integer;
                            }
                            else if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.separator;
                            }
                            else
                            {
                                switch (_coords[i])
                                {
                                    case '.':
                                        _newState = NumState.decPlace;
                                        break;
                                    case 'E':
                                    case 'e':
                                        _newState = NumState.exponent;
                                        break;
                                    case '+':
                                    case '-':
                                        _newState = NumState.prefix;
                                        break;
                                    default:
                                        _newState = NumState.invalid;
                                        break;
                                }
                            }
                            break;
                        case NumState.decPlace:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.fraction;
                            }
                            else if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.separator;
                            }
                            else
                            {
                                switch (_coords[i])
                                {
                                    case 'E':
                                    case 'e':
                                        _newState = NumState.exponent;
                                        break;
                                    case '+':
                                    case '-':
                                        _newState = NumState.prefix;
                                        break;
                                    default:
                                        _newState = NumState.invalid;
                                        break;
                                }
                            }
                            break;
                        case NumState.fraction:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.fraction;
                            }
                            else if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.separator;
                            }
                            else
                            {
                                switch (_coords[i])
                                {
                                    case '.':
                                        _newState = NumState.decPlace;
                                        break;
                                    case 'E':
                                    case 'e':
                                        _newState = NumState.exponent;
                                        break;
                                    case '+':
                                    case '-':
                                        _newState = NumState.prefix;
                                        break;
                                    default:
                                        _newState = NumState.invalid;
                                        break;
                                }
                            }
                            break;
                        case NumState.exponent:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.expValue;
                            }
                            else if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.invalid;
                            }
                            else
                            {
                                switch (_coords[i])
                                {
                                    case '+':
                                    case '-':
                                        _newState = NumState.expPrefix;
                                        break;
                                    default:
                                        _newState = NumState.invalid;
                                        break;
                                }
                            }
                            break;
                        case NumState.expPrefix:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.expValue;
                            }
                            else
                            {
                                _newState = NumState.invalid;
                            }
                            break;
                        case NumState.expValue:
                            if (char.IsNumber(_coords[i]))
                            {
                                _newState = NumState.expValue;
                            }
                            else if (IsCoordSeparator(_coords[i]))
                            {
                                _newState = NumState.separator;
                            }
                            else
                            {
                                switch (_coords[i])
                                {
                                    case '.':
                                        _newState = NumState.decPlace;
                                        break;
                                    case '+':
                                    case '-':
                                        _newState = NumState.prefix;
                                        break;
                                    default:
                                        _newState = NumState.invalid;
                                        break;
                                }
                            }
                            break;
                    }

                    if (_newState < _currState)
                    {
                        result = float.Parse(_coords.Substring(_pos, i - _pos), NumberStyles.Float, CultureInfo.InvariantCulture);
                        _pos = i;
                        _currState = _newState;
                        return MarkState(true);
                    }
                    else if (_newState != _currState && _currState == NumState.separator)
                    {
                        _pos = i;
                    }

                    if (_newState == NumState.invalid)
                    {
                        result = float.MinValue;
                        return MarkState(false);
                    }
                    _currState = _newState;
                    i++;
                }

                if (_currState == NumState.separator || !_parseWorked || _pos >= _coords.Length)
                {
                    result = float.MinValue;
                    return MarkState(false);
                }
                else
                {
                    result = float.Parse(_coords.Substring(_pos, _coords.Length - _pos), NumberStyles.Float, CultureInfo.InvariantCulture);
                    _pos = _coords.Length;
                    return MarkState(true);
                }
            }

            private static bool IsCoordSeparator(char value)
            {
                switch (value)
                {
                    case ' ':
                    case '\t':
                    case '\n':
                    case '\r':
                    case ',':
                        return true;
                }
                return false;
            }
        }

        public static List<ISegment> Parse(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException("path");
            }

            var segments = new List<ISegment>();

            try
            {
                char command;
                bool isRelative;

                foreach (var commandSet in SplitCommands(path.TrimEnd(null)))
                {
                    command = commandSet[0];
                    isRelative = char.IsLower(command);

                    CreatePathSegment(command, segments, new CoordinateParser(commandSet.Trim()), isRelative);
                }
            }
            catch (Exception exc)
            {
                //Trace.TraceError("Error parsing path \"{0}\": {1}", path, exc.Message);
            }

            return segments;
        }

        private static void CreatePathSegment(char command, List<ISegment> segments, CoordinateParser parser, bool isRelative)
        {
            var coords = new float[6];
            
            switch (command)
            {
                case 'm': // relative moveto
                case 'M': // moveto
                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]))
                    {
                        if (segments.Count>0)
                            segments.Add(new LinearSegment(segments.Last().End, ToAbsolute(coords[0], coords[1], segments, isRelative)));
                        else
                            segments.Add(new MoveToSegment(ToAbsolute(coords[0], coords[1], segments, isRelative)));
                    }
                    break;
                case 'a':
                case 'A':
                    throw new NotImplementedException();
                    /*
                    bool size;
                    bool sweep;

                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]) &&
                           parser.TryGetFloat(out coords[2]) && parser.TryGetBool(out size) &&
                           parser.TryGetBool(out sweep) && parser.TryGetFloat(out coords[3]) &&
                           parser.TryGetFloat(out coords[4]))
                    {
                        // A|a rx ry x-axis-rotation large-arc-flag sweep-flag x y
                        segments.Add(new SvgArcSegment(segments.Last().End, coords[0], coords[1], coords[2],
                            (size ? SvgArcSize.Large : SvgArcSize.Small),
                            (sweep ? SvgArcSweep.Positive : SvgArcSweep.Negative),
                            ToAbsolute(coords[3], coords[4], segments, isRelative)));
                    }*/
                    break;
                case 'l': // relative lineto
                case 'L': // lineto
                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]))
                    {
                        segments.Add(new LinearSegment(segments.Last().End,
                            ToAbsolute(coords[0], coords[1], segments, isRelative)));
                    }
                    break;
                case 'H': // horizontal lineto
                case 'h': // relative horizontal lineto
                    while (parser.TryGetFloat(out coords[0]))
                    {
                        segments.Add(new LinearSegment(segments.Last().End,
                            ToAbsolute(coords[0], segments.Last().End.Y, segments, isRelative, false)));
                    }
                    break;
                case 'V': // vertical lineto
                case 'v': // relative vertical lineto
                    while (parser.TryGetFloat(out coords[0]))
                    {
                        segments.Add(new LinearSegment(segments.Last().End,
                            ToAbsolute(segments.Last().End.X, coords[0], segments, false, isRelative)));
                    }
                    break;
                case 'Q': // curveto
                case 'q': // relative curveto
                    throw new NotImplementedException();
                    /*
                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]) &&
                           parser.TryGetFloat(out coords[2]) && parser.TryGetFloat(out coords[3]))
                    {
                        segments.Add(new SvgQuadraticCurveSegment(segments.Last().End,
                            ToAbsolute(coords[0], coords[1], segments, isRelative),
                            ToAbsolute(coords[2], coords[3], segments, isRelative)));
                    }*/
                    break;
                case 'T': // shorthand/smooth curveto
                case 't': // relative shorthand/smooth curveto
                    throw new NotImplementedException();
                    /*
                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]))
                    {
                        var lastQuadCurve = segments.Last() as SvgQuadraticCurveSegment;

                        var controlPoint = lastQuadCurve != null
                            ? Reflect(lastQuadCurve.ControlPoint, segments.Last().End)
                            : segments.Last().End;

                        segments.Add(new SvgQuadraticCurveSegment(segments.Last.End, controlPoint,
                            ToAbsolute(coords[0], coords[1], segments, isRelative)));
                    }*/
                    break;
                case 'C': // curveto
                case 'c': // relative curveto
                    throw new NotImplementedException();
                    /*
                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]) &&
                           parser.TryGetFloat(out coords[2]) && parser.TryGetFloat(out coords[3]) &&
                           parser.TryGetFloat(out coords[4]) && parser.TryGetFloat(out coords[5]))
                    {
                        segments.Add(new BezierCurve(segments.Last().End,
                            ToAbsolute(coords[0], coords[1], segments, isRelative),
                            ToAbsolute(coords[2], coords[3], segments, isRelative),
                            ToAbsolute(coords[4], coords[5], segments, isRelative)));
                    }*/
                    break;
                case 'S': // shorthand/smooth curveto
                case 's': // relative shorthand/smooth curveto
                    throw new NotImplementedException();
                    /*
                    while (parser.TryGetFloat(out coords[0]) && parser.TryGetFloat(out coords[1]) &&
                           parser.TryGetFloat(out coords[2]) && parser.TryGetFloat(out coords[3]))
                    {
                        var lastCubicCurve = segments.Last();

                        var controlPoint = lastCubicCurve != null
                            ? Reflect(lastCubicCurve.p1, segments.Last().End)
                            : segments.Last().End;

                        segments.Add(new BezierCurve(segments.Last().End, controlPoint,
                            ToAbsolute(coords[0], coords[1], segments, isRelative),
                            ToAbsolute(coords[2], coords[3], segments, isRelative)));
                    }*/
                    break;
                case 'Z': // closepath
                case 'z': // relative closepath
                    segments.Add(new LinearSegment(segments.Last().End,segments.First().Start));
                    break;
            }
        }

        private static Vector2 Reflect(Vector2 point, Vector2 mirror)
        {
            float x, y, dx, dy;
            dx = Math.Abs(mirror.X - point.X);
            dy = Math.Abs(mirror.Y - point.Y);

            if (mirror.X >= point.X)
            {
                x = mirror.X + dx;
            }
            else
            {
                x = mirror.X - dx;
            }
            if (mirror.Y >= point.Y)
            {
                y = mirror.Y + dy;
            }
            else
            {
                y = mirror.Y - dy;
            }

            return new Vector2(x, y);
        }

        /// <summary>
        /// Creates point with absolute coorindates.
        /// </summary>
        /// <param name="x">Raw X-coordinate value.</param>
        /// <param name="y">Raw Y-coordinate value.</param>
        /// <param name="segments">Current path segments.</param>
        /// <param name="isRelativeBoth"><b>true</b> if <paramref name="x"/> and <paramref name="y"/> contains relative coordinate values, otherwise <b>false</b>.</param>
        /// <returns><see cref="Vector2"/> that contains absolute coordinates.</returns>
        private static Vector2 ToAbsolute(float x, float y, List<ISegment> segments, bool isRelativeBoth)
        {
            return ToAbsolute(x, y, segments, isRelativeBoth, isRelativeBoth);
        }

        /// <summary>
        /// Creates point with absolute coorindates.
        /// </summary>
        /// <param name="x">Raw X-coordinate value.</param>
        /// <param name="y">Raw Y-coordinate value.</param>
        /// <param name="segments">Current path segments.</param>
        /// <param name="isRelativeX"><b>true</b> if <paramref name="x"/> contains relative coordinate value, otherwise <b>false</b>.</param>
        /// <param name="isRelativeY"><b>true</b> if <paramref name="y"/> contains relative coordinate value, otherwise <b>false</b>.</param>
        /// <returns><see cref="Vector2"/> that contains absolute coordinates.</returns>
        private static Vector2 ToAbsolute(float x, float y, List<ISegment> segments, bool isRelativeX, bool isRelativeY)
        {
            var point = new Vector2(x, y);

            if ((isRelativeX || isRelativeY) && segments.Count > 0)
            {
                var lastSegment = segments.Last();

                // if the last element is a SvgClosePathSegment the position of the previous element should be used because the position of SvgClosePathSegment is 0,0
                throw new NotImplementedException("need to fix the line below");
                //if (lastSegment is SvgClosePathSegment) lastSegment = segments.Reverse().OfType<SvgMoveToSegment>().First();

                if (isRelativeX)
                {
                    point.X += lastSegment.End.X;
                }

                if (isRelativeY)
                {
                    point.Y += lastSegment.End.Y;
                }
            }

            return point;
        }

        private static IEnumerable<string> SplitCommands(string path)
        {
            var commandStart = 0;

            for (var i = 0; i < path.Length; i++)
            {
                string command;
                if (char.IsLetter(path[i]) && path[i] != 'e') //e is used in scientific notiation. but not svg path
                {
                    command = path.Substring(commandStart, i - commandStart).Trim();
                    commandStart = i;

                    if (!string.IsNullOrEmpty(command))
                    {
                        yield return command;
                    }

                    if (path.Length == i + 1)
                    {
                        yield return path[i].ToString();
                    }
                }
                else if (path.Length == i + 1)
                {
                    command = path.Substring(commandStart, i - commandStart + 1).Trim();

                    if (!string.IsNullOrEmpty(command))
                    {
                        yield return command;
                    }
                }
            }
        }
    }
}

public class SVGPath2BezierCurve
{
    public SVGConverter.BezierCurve[] ConvertToBezier(string svgPath)
    {
        /*SvgDocument doc = SvgDocument.FromSvg<SvgDocument>(svgPath);
        SvgPath path = null;
        for (int i=0;i<path.PathData.Count;++i)
        {
            SvgPathSegment seg = path.PathData[i];
            if (seg is SvgMoveToSegment)
            {
                SvgMoveToSegment s = seg as SvgMoveToSegment;
                s.Start
            }
        }*/
        /*float x, y, x1, y1, x2, y2, x0, y0;

            for (var i = 0, len = segs.numberOfItems; i < len; ++i)
            {
                var seg = segs.getItem(i), c = seg.pathSegTypeAsLetter;
                switch (c)
                {
                    // Move
                    case 'M':
                        x = x1 = x2 = 0;
                        y = y1 = y2 = 0;
                        x0 = seg.x;
                        y0 = seg.y;
                        bezier.push({ x: 0,y: 0});
            break;
  case 'm':
    // Have not seen this in a path, yet
    break;
  // Line segment
  case 'L':
    bezier.push({ x: x,y: y});
            bezier.push({ x: (seg.x - x0),y: (seg.y - y0)});
            bezier.push({ x: (seg.x - x0),y: (seg.y - y0)}); ;
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  case 'l':
    bezier.push({ x: x,y: y});
            bezier.push({ x: (x + seg.x),y: (y + seg.y)});
            bezier.push({ x: (x + seg.x),y: (y + seg.y)}); ;
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  // Cubic
  case 'C':
    bezier.push({ x: (seg.x1 - x0),y: (seg.y1 - y0)});
            bezier.push({ x: (seg.x2 - x0),y: (seg.y2 - y0)});
            bezier.push({ x: (seg.x - x0),y: (seg.y - y0)});

            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  case 'c':
    bezier.push({ x: (x + seg.x1),y: (y + seg.y1)})
    bezier.push({ x: (x + seg.x2),y: (y + seg.y2)});
            bezier.push({ x: (x + seg.x),y: (y + seg.y)}); ;

            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  // Smooth
  case 'S':
    bezier.push({ x: ((x + (x - x2))),y: ((y + (y - y2)))});
            bezier.push({ x: (seg.x2 - x0),y: (seg.y2 - y0)});
            bezier.push({ x: (seg.x - x0),y: (seg.y - y0)}); ;

            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  case 's':
    bezier.push({ x: (x + (x - x2)),y: (y + (y - y2))});
            bezier.push({ x: (x + seg.x2),y: (y + seg.y2)});
            bezier.push({ x: (x + seg.x),y: (y + seg.y)}); ;
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  // Quadratic
  case 'Q':
    break;
  case 'q':
    break;

  // Horizontal
  case 'H':
    bezier.push({ x: x,y: y});
            bezier.push({ x: (seg.x - x0),y: y});
            bezier.push({ x: (seg.x - x0),y: y});
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  case 'h':
    bezier.push({ x: x,y: y});
            bezier.push({ x: (seg.x + x),y: y});
            bezier.push({ x: (seg.x + x),y: y});
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  // Vertical
  case 'V':
    bezier.push({ x: x,y: y});
            bezier.push({ x: x,y: (seg.y - y0)});
            bezier.push({ x: x,y: (seg.y - y0)});
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  case 'v':
    bezier.push({ x: x,y: y});
            bezier.push({ x: x,y: (seg.y + y)});
            bezier.push({ x: x,y: (seg.y + y)});
            x = bezier[bezier.length - 1].x;
            y = bezier[bezier.length - 1].y;
            x1 = bezier[bezier.length - 3].x;
            y1 = bezier[bezier.length - 3].y;
            x2 = bezier[bezier.length - 2].x;
            y2 = bezier[bezier.length - 2].y;
            break;
  // Close
  case 'Z': 
    break;
  case 'z': 
    break;


  case 't': 
  case 'a': 
}
    }

return bezier;*/
        return null;
    }
}
