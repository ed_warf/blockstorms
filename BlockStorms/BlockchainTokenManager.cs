﻿using System;
using System.Collections.Generic;

namespace BlockStorms
{
    public class SVGToken
    {
        public string Name { get; private set; }
        private string IPFSHash;

        public SVGToken(string name, string IPFSHash)
        {
            Name = name;
            this.IPFSHash = IPFSHash;
        }

        public string GetSVGString()
        {
            string rectString =
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" +
"<!-- Created with Inkscape (http://www.inkscape.org/) -->" +
"" +
"<svg" +
"   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"" +
"   xmlns:cc=\"http://creativecommons.org/ns#\"" +
"   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" +
"   xmlns:svg=\"http://www.w3.org/2000/svg\"" +
"   xmlns=\"http://www.w3.org/2000/svg\"" +
"   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"" +
"   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"" +
"   version=\"1.1\"" +
"   id=\"svg5097\"" +
"   width=\"800\"" +
"   height=\"800\"" +
"   viewBox=\"0 0 800 800\"" +
"   sodipodi:docname=\"ethereum-ETHBerlin.svg\"" +
"   inkscape:version=\"0.92.3 (2405546, 2018-03-11)\">" +
"  <metadata" +
"     id=\"metadata5103\">" +
"    <rdf:RDF>" +
"      <cc:Work" +
"         rdf:about=\"\">" +
"        <dc:format>image/svg+xml</dc:format>" +
"        <dc:type" +
"           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />" +
"        <dc:title></dc:title>" +
"      </cc:Work>" +
"    </rdf:RDF>" +
"  </metadata>" +
"  <defs" +
"     id=\"defs5101\" />" +
"  <sodipodi:namedview" +
"     pagecolor=\"#ffffff\"" +
"     bordercolor=\"#666666\"" +
"     borderopacity=\"1\"" +
"     objecttolerance=\"10\"" +
"     gridtolerance=\"10\"" +
"     guidetolerance=\"10\"" +
"     inkscape:pageopacity=\"0\"" +
"     inkscape:pageshadow=\"2\"" +
"     inkscape:window-width=\"2560\"" +
"     inkscape:window-height=\"1346\"" +
"     id=\"namedview5099\"" +
"     showgrid=\"false\"" +
"     inkscape:zoom=\"1.40375\"" +
"     inkscape:cx=\"400\"" +
"     inkscape:cy=\"400\"" +
"     inkscape:window-x=\"-11\"" +
"     inkscape:window-y=\"-11\"" +
"     inkscape:window-maximized=\"1\"" +
"     inkscape:current-layer=\"svg5097\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 0,0 400,0\"" +
"     id=\"path5121\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 400,400 0,400 0,0\"" +
"     id=\"path5123\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 600,0 600,600 0,600 z\"" +
"     id=\"path5125\"" +
"     inkscape:connector-curvature=\"0\" />"+
"</svg>";
            if (IPFSHash == "rectangle")
                return rectString;
            string svgString =
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" +
"<!-- Created with Inkscape (http://www.inkscape.org/) -->" +
"" +
"<svg" +
"   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"" +
"   xmlns:cc=\"http://creativecommons.org/ns#\"" +
"   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" +
"   xmlns:svg=\"http://www.w3.org/2000/svg\"" +
"   xmlns=\"http://www.w3.org/2000/svg\"" +
"   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"" +
"   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"" +
"   version=\"1.1\"" +
"   id=\"svg5097\"" +
"   width=\"800\"" +
"   height=\"800\"" +
"   viewBox=\"0 0 800 800\"" +
"   sodipodi:docname=\"ethereum-ETHBerlin.svg\"" +
"   inkscape:version=\"0.92.3 (2405546, 2018-03-11)\">" +
"  <metadata" +
"     id=\"metadata5103\">" +
"    <rdf:RDF>" +
"      <cc:Work" +
"         rdf:about=\"\">" +
"        <dc:format>image/svg+xml</dc:format>" +
"        <dc:type" +
"           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />" +
"        <dc:title></dc:title>" +
"      </cc:Work>" +
"    </rdf:RDF>" +
"  </metadata>" +
"  <defs" +
"     id=\"defs5101\" />" +
"  <sodipodi:namedview" +
"     pagecolor=\"#ffffff\"" +
"     bordercolor=\"#666666\"" +
"     borderopacity=\"1\"" +
"     objecttolerance=\"10\"" +
"     gridtolerance=\"10\"" +
"     guidetolerance=\"10\"" +
"     inkscape:pageopacity=\"0\"" +
"     inkscape:pageshadow=\"2\"" +
"     inkscape:window-width=\"2560\"" +
"     inkscape:window-height=\"1346\"" +
"     id=\"namedview5099\"" +
"     showgrid=\"false\"" +
"     inkscape:zoom=\"1.40375\"" +
"     inkscape:cx=\"400\"" +
"     inkscape:cy=\"400\"" +
"     inkscape:window-x=\"-11\"" +
"     inkscape:window-y=\"-11\"" +
"     inkscape:window-maximized=\"1\"" +
"     inkscape:current-layer=\"svg5097\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 401.06857,49.154052 602.67142,381.83437\"" +
"     id=\"path5121\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 604.09617,413.89136 401.78094,701.6919 195.90383,414.60374\"" +
"     id=\"path5123\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 199.46572,380.40962 401.06857,49.154052 399.64381,498.66429\"" +
"     id=\"path5125\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 401.78094,701.6919 400.35619,535.70793 604.09617,413.89136\"" +
"     id=\"path5127\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 602.67142,381.83437 399.64381,498.66429 199.46572,380.40962\"" +
"     id=\"path5129\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 195.90383,414.60374 400.35619,535.70793\"" +
"     id=\"path5131\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"m 199.46572,380.40962 200.17809,-90.47195 203.02761,91.8967\"" +
"     id=\"path5133\"" +
"     inkscape:connector-curvature=\"0\" />" +
"</svg>";

            return svgString;
            //throw new NotImplementedException();
        }
    }
    class BlockchainTokenManager
    {
        public static List<SVGToken> GetAvailableSVGTokens()
        {
            List<SVGToken> tokens = new List<SVGToken>();
            //TODO: implement this
            tokens.Add(new SVGToken("TESTToken", "0xdeadbeef"));
            tokens.Add(new SVGToken("Rectangle", "rectangle"));
            return tokens;
        }
    }
}
