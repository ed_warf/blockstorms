using MonoBrickFirmware.Display;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace BlockStorms.RpcModel
{
    public class LegacyRpcClient
    {
        private readonly Uri _baseUrl;
        private readonly JsonSerializerSettings _jsonSerializerSettings;
        private object _lockObject = new object();
        private const int NUMBER_OF_SECONDS_TO_RECREATE_HTTP_CLIENT = 60;

        public LegacyRpcClient(Uri baseUrl)
            : this(baseUrl, null)
        {
        }

        public LegacyRpcClient(Uri baseUrl,
            JsonSerializerSettings jsonSerializerSettings)
        {
            _baseUrl = baseUrl;
            if (jsonSerializerSettings == null)
            {
                //jsonSerializerSettings = DefaultJsonSerializerSettingsFactory.BuildDefaultJsonSerializerSettings();
            }
            _jsonSerializerSettings = jsonSerializerSettings;

        }

        /// <summary>
        /// Calls WebRequest using Unity model. Must be called from coroutine on main thread. Returns RpcResult<TResult>
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        public TResult SendRequest<TResult>(RpcRequest request)
        {
            JsonSerializerSettings settings = null;
            //LcdConsole.WriteLine("serizalizing request...");
            var rpcRequestJson = JsonConvert.SerializeObject(request, settings);
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_baseUrl);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                //LcdConsole.WriteLine("obtaining stream...");

                try
                {
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                       // LcdConsole.WriteLine("writing stream...");
                        streamWriter.Write(rpcRequestJson);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }
                catch(Exception ex)
                {
                    //MonoBrickDebugHelper.WriteText(ex.Message);                   

                    ////LcdConsole.WriteLine("Press Enter to continue");
                    //MonoBrickDebugHelper.WaitForEnter();
                    //throw ex;
                }

               // LcdConsole.WriteLine("wait for response...");
                using (var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                       // LcdConsole.WriteLine("read response...");
                        var result = streamReader.ReadToEnd();
                       // LcdConsole.WriteLine("parse response...");
                        RpcResponse responseObject = JsonConvert.DeserializeObject<RpcResponse>(result, settings);
                        //LcdConsole.WriteLine("get result...");
                        return responseObject.GetResult<TResult>(true, settings);
                    }
                }
            }
        }

        public bool CheckConnection()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_baseUrl);
                request.Method = "HEAD";
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                LcdConsole.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
