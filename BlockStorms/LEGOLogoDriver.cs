﻿using SVGConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BlockStorms
{
    class LEGOLogoDriver : IDrawContext
    {
        public Vector2 CurrentPos { get; private set; } = new Vector2(0, 0);

        public Vector2 CurrentDir { get; private set; } = new Vector2(0, 1);

        public LEGOLogoDriver(){}

        private void GoForward(float distance)
        {
            BrickSettings.MotorSetup.LeftMotor.SetSpeed((sbyte)-BrickSettings.LinearSpeed);
            BrickSettings.MotorSetup.RightMotor.SetSpeed((sbyte)-BrickSettings.LinearSpeed);
            int msTime = (int)(distance * BrickSettings.LinearSpeed * BrickSettings.GeneralScale / 100.0f);
            Thread.Sleep(msTime);
            BrickSettings.MotorSetup.LeftMotor.Brake();
            BrickSettings.MotorSetup.RightMotor.Brake();
        }

        public void PenDown()
        {
            //throw new NotImplementedException();
        }

        public void PenUp()
        {
            //throw new NotImplementedException();
        }

        private void TurnCW(float angle)
        {
            BrickSettings.MotorSetup.LeftMotor.SetSpeed((sbyte)-BrickSettings.AngularSpeed);
            BrickSettings.MotorSetup.RightMotor.SetSpeed(BrickSettings.AngularSpeed);
            int msTime = (int)(1000 * 3 * angle / (2 * Math.PI));
            Thread.Sleep(msTime);
            BrickSettings.MotorSetup.LeftMotor.Brake();
            BrickSettings.MotorSetup.RightMotor.Brake();
        }

        public void MoveTo(Vector2 p)
        {
            //check angle between current direction and target direction
            Vector2 newDir = p - CurrentPos;
            float length = newDir.SafeNormalize(CurrentDir);
            float angle = (float)Math.Acos(Math.Max(Math.Min(newDir.Dot(CurrentDir),1),-1));
            TurnCW(angle);
            CurrentDir = newDir;
            GoForward(length);
            CurrentPos = p;
        }
    }
}
