﻿using System.Threading;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Display.Menus;

namespace BlockStorms
{
    public class ItemWithLabel : ChildItem
    {
        private string text;
        private const int rightArrowOffset = 4;
        private const int arrowEdge = 4;
        private const int holdSleepTime = 100;
        private const int holdSingleWait = 5;
        private const int holdTenWait = 25;
        private const int holdHundredWait = 45;
        private const int holdFiveHundredWait = 75;

        private Font font;
        private Rectangle rect;
        private CancellationTokenSource cancelSource = new CancellationTokenSource();
        
        public ItemWithLabel(string text, int startValue)
        {
            this.text = text;
            this.Value = startValue;
        }

        public override void OnDrawTitle(Font f, Rectangle r, bool color)
        {
            font = f;
            rect = r;

            int arrowWidth = (int)f.maxWidth / 4;

            string valueAsString = " " + Value.ToString() + " ";
            Point p = f.TextSize(valueAsString);
            Rectangle numericRect = new Rectangle(new Point(Lcd.Width - p.X, r.P1.Y), r.P2);
            Rectangle textRect = new Rectangle(new Point(r.P1.X, r.P1.Y), new Point(r.P2.X - (p.X), r.P2.Y));
            Rectangle leftArrowRect = new Rectangle(new Point(numericRect.P1.X, numericRect.P1.Y + arrowEdge), new Point(numericRect.P1.X + arrowWidth, numericRect.P2.Y - arrowEdge));
            Rectangle rightArrowRect = new Rectangle(new Point(numericRect.P2.X - (arrowWidth + rightArrowOffset), numericRect.P1.Y + arrowEdge), new Point(numericRect.P2.X - rightArrowOffset, numericRect.P2.Y - arrowEdge));

            Lcd.WriteTextBox(f, textRect, text, color, Lcd.Alignment.Left);
            Lcd.WriteTextBox(f, numericRect, valueAsString, color, Lcd.Alignment.Right);
            Lcd.DrawArrow(leftArrowRect, Lcd.ArrowOrientation.Left, color);
            Lcd.DrawArrow(rightArrowRect, Lcd.ArrowOrientation.Right, color);
        }

        public override void OnHideContent()
        {
            cancelSource.Cancel();
        }

        public int Value { get; set; }
    }
    public class CalibrationView : Menu
    {
        int testTimeMs = 1000;
        ItemWithLabel labelToUpdate;
        public CalibrationView() : base("Configuration menu")
        {
            labelToUpdate = new ItemWithLabel("CurrentPenPos", BrickSettings.MotorSetup.VMotor.GetTachoCount());
            AddItem(new ItemWithNumericInput("Testing time(ms)", testTimeMs, (value)=> { testTimeMs = value; }, -5000, 5000));
            AddItem(new ItemWithNumericInput("General scale", BrickSettings.GeneralScale, (value)=> { BrickSettings.GeneralScale = value; }, 0,1000));
            AddItem(new ItemWithNumericInput("Angular speed", BrickSettings.AngularSpeed, (value) => { BrickSettings.AngularSpeed = (sbyte)value; }, -100, 100, TestAngularSpeed));
            AddItem(new ItemWithNumericInput("MinPenPos", BrickSettings.MinPenPos, (value) => { BrickSettings.MinPenPos = value; }, -5000, 5000, TestMinPenPos));
            AddItem(new ItemWithNumericInput("MaxPenPos", BrickSettings.MaxPenPos, (value) => { BrickSettings.MaxPenPos = value; }, -5000, 5000, TestMaxPenPos));
                
            AddItem(labelToUpdate);
        }

        private void OnLinearSpeedChanged(int value)
        {
            BrickSettings.LinearSpeed = (sbyte)value;
        }

        private void TestLinearSpeed()
        {
            BrickSettings.MotorSetup.LeftMotor.SetSpeed(BrickSettings.LinearSpeed);
            BrickSettings.MotorSetup.RightMotor.SetSpeed(BrickSettings.LinearSpeed);
            Thread.Sleep(1000);
            BrickSettings.MotorSetup.LeftMotor.Brake();
            BrickSettings.MotorSetup.RightMotor.Brake();
        }

        private void TestAngularSpeed()
        {
            BrickSettings.MotorSetup.LeftMotor.SetSpeed((sbyte)-BrickSettings.AngularSpeed);
            BrickSettings.MotorSetup.RightMotor.SetSpeed(BrickSettings.AngularSpeed);
            Thread.Sleep(testTimeMs);
            BrickSettings.MotorSetup.LeftMotor.Brake();
            BrickSettings.MotorSetup.RightMotor.Brake();
        }

        private void TestMinPenPos()
        {
            BrickSettings.MotorSetup.VMotor.SetSpeed(-10);
            while (BrickSettings.MotorSetup.VMotor.GetTachoCount() > BrickSettings.MinPenPos)
                labelToUpdate.Value = BrickSettings.MotorSetup.VMotor.GetTachoCount();
            BrickSettings.MotorSetup.VMotor.Brake();
        }

        private void TestMaxPenPos()
        {
            BrickSettings.MotorSetup.VMotor.SetSpeed(10);
            while (BrickSettings.MotorSetup.VMotor.GetTachoCount() < BrickSettings.MaxPenPos)
                labelToUpdate.Value = BrickSettings.MotorSetup.VMotor.GetTachoCount();
            BrickSettings.MotorSetup.VMotor.Brake();
        }
    }
}
