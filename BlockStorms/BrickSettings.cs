﻿using MonoBrickFirmware.Movement;

namespace BlockStorms
{
    public sealed class MotorSetup
    {
        public Motor LeftMotor;
        public Motor RightMotor;
        public Motor VMotor;
    }
    static class BrickSettings
    {
        public static sbyte LinearSpeed { get; set; } = 30;
        public static sbyte AngularSpeed { get; set; } = 30;
        public static int MinPenPos { get; set; } = 0;
        public static int MaxPenPos { get; set; } = 500;
        public static int GeneralScale { get; set; } = 5;

        public static MotorSetup MotorSetup { get; set; }
    }
}
