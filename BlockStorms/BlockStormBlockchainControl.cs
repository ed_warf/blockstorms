﻿using MonoBrickFirmware.Display;
using MonoBrickFirmware.Display.Menus;
using SVGConverter;
using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace BlockStorms
{
    internal class ExecuteDrawItem : ChildItem
    {
        private SVGToken token;
        private readonly bool useEscToStop;

        public ExecuteDrawItem(SVGToken token, bool useEscToStop) : base(token.Name)
        {
            this.token = token;
            this.useEscToStop = useEscToStop;
        }

        public override void OnEnterPressed()
        {
            StartProgram();
        }

        private void StartProgram()
        {
            Lcd.Clear();
            LcdConsole.WriteLine("Downloading SVG image....");
            string svgString = token.GetSVGString();
           
            XElement svg = XElement.Parse(svgString);
            XName pName = XName.Get("path", @"http://www.w3.org/2000/svg");
            IDrawContext drawer = new LEGOLogoDriver();
            foreach (var elem in svg.Elements(pName))
            {
                List<ISegment> segments = SvgPathBuilder.Parse(elem.Attribute("d").Value.ToString());
                //actual drawing is here
                foreach(ISegment seg in segments)
                {
                    seg.Draw(drawer);
                }
            }
        }

        public void Start(IParentItem parent)
        {
            Parent = parent;
            Parent.SetFocus(this);
            StartProgram();
        }

        public override void OnEscPressed()
        {
            //ProgramManager.StopProgram(this.program);
        }

        private void OnDone(Exception e)
        {
            if (!useEscToStop)
            {
                Parent.ResumeButtonEvents();
            }
            Parent.RemoveFocus(this);
        }
    }

    public class BlockStormBlockchainControl : ItemList
    {
        private bool useEscToStop;

        public BlockStormBlockchainControl() : base("Wallet", Font.MediumFont, true, "No tokens found")
        {
            this.useEscToStop = true;
        }

        protected override List<IChildItem> OnCreateChildList()
        {
            List<SVGToken> tokens = BlockchainTokenManager.GetAvailableSVGTokens();
            var childList = new List<IChildItem>();
            foreach (var tok in tokens)
            {
                childList.Add(new ExecuteDrawItem(tok, useEscToStop));
            }
            return childList;
        }
    }
}
