using BlockStorms.RpcModel;
using MonoBrickFirmware.Display;
using MonoBrickFirmware.Display.Menus;
using MonoBrickFirmware.Movement;
using MonoBrickFirmware.UserInput;
using Newtonsoft.Json;
using Org.BouncyCastle.Math;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace BlockStorms
{
    class Config
    {
        [JsonProperty("ethereum")]
        public EthereumConfig EthereumConfig { get; set; }

        [JsonProperty("ipfs")]
        public IPFSConfig IPFSConfig { get; set; }
    }

    public class EthereumConfig
    {
        public string Host;
        public uint Port;
        public string ContractAddress;
        public string UserAddress;
    }

    public class IPFSConfig
    { 
        public string Host;
        public uint Port;
    }

    class MonoBrickDebugHelper
    {
        public static bool MessageYesNo(string text)
        {
            bool continueExecution = false;
            bool stopExecution = false;

            LcdConsole.WriteLine(text);            

            using (ButtonEvents buts = new ButtonEvents())
            {
                buts.EscapePressed += () =>
                {
                    stopExecution = true;
                    LcdConsole.WriteLine("EscapePressed");
                };
                buts.EnterPressed += () =>
                {
                    continueExecution = true;
                    LcdConsole.WriteLine("EnterPressed");
                };
                while (!continueExecution && !stopExecution)
                {
                    Thread.Sleep(200);
                }

                return continueExecution;
            }
        }

        public static void WaitForEnter()
        {
            bool wait = true;

            using (ButtonEvents buts = new ButtonEvents())
            {
                buts.EnterPressed += () =>
                {
                    wait = false;
                };
                while (wait)
                {
                    Thread.Sleep(200);
                }
            }
        }

        public static void WriteText(string message)
        {
            const int maxLineLength = 25;
            string[] words = message.Split(' ');
            string line = words[0];
            for (int i=1;i< words.Length;++i)
            {
                string nextWord = words[i];
                if ((line.Length + nextWord.Length + 1) >= maxLineLength)
                {
                    LcdConsole.WriteLine(line);
                    line = nextWord;
                }
                else
                {
                    line = line + " " + nextWord;
                }
            }
            LcdConsole.WriteLine(line);
        }
    }


	class MainClass
    {
		public static string ConfigFilePath = @"/usr/local/bin/blockstormsconfig.json";
		//public static string ConfigFilePath = "blockstormsconfig.json";

        static Random random = new Random();

        const int stop_distance = 30;
        static bool escapePressed = false;
        
        static int distance = 100;

        static Config config;
        static IPFSClient ipfsClient;
        static LegacyRpcClient rpcClient;

        public static byte[] EncodeAddress(BigInteger value)
        {
            var bytes = value.ToByteArray();
            if(BitConverter.IsLittleEndian)
            {
                Array.Reverse(bytes);
            }

            if (bytes.Length == 33)
            {
                if (bytes[0] == 0x00)
                {
                    bytes = bytes.Skip(1).ToArray();
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }

            const int maxIntSizeInBytes = 32;

            var ret = new byte[maxIntSizeInBytes];

            for (var i = 0; i < ret.Length; i++)
                ret[i] = 0;

            Array.Copy(bytes, 0, ret, maxIntSizeInBytes - bytes.Length, bytes.Length);

            return ret;
        }

        

        public static BigInteger[] GetItems()
        {
            //LcdConsole.WriteLine("Connecting to the blockchain...");
            //LcdConsole.WriteLine(EthUtils.CalcFunctionSignature("balanceOf()"));


            //bool isAvailable = client.CheckConnection();

            Dictionary<string, string> args = new Dictionary<string, string>();

            args["to"] = config.EthereumConfig.ContractAddress;
            args["data"] = EthUtils.GetData("balanceOf(address)", new string[] { EthUtils.PaddedAddress(config.EthereumConfig.UserAddress) } );

            RpcRequest req = new RpcRequest(Guid.NewGuid().ToString(), "eth_call", args);
            //LcdConsole.WriteLine("Sending request...");
            string response = rpcClient.SendRequest<string>(req);
            //LcdConsole.WriteLine(String.Format("Found {0} token.", response));

            List<BigInteger> tokenIds = new List<BigInteger>();
            BigInteger count = new BigInteger(EthUtils.RemoveHexPrefix(response));
            for (BigInteger i = BigInteger.Zero; i.CompareTo(count) < 0; i = i.Add(BigInteger.One))
            {
                args["data"] = EthUtils.GetData("tokenByIndex(uint256)", new string[] { i.ToString(16).PadLeft(64, '0') });
                req = new RpcRequest(Guid.NewGuid().ToString(), "eth_call", args);
                response = rpcClient.SendRequest<string>(req);
                tokenIds.Add(new BigInteger(EthUtils.RemoveHexPrefix(response)));
            }

            return tokenIds.ToArray();
        }

        public static string DownloadData(string tokenId)
        {

            return "";
        }

        public static bool TestRestConnection()
        {
            string connectionURL = "http://localhost";
            LcdConsole.WriteLine("Test connection...");
            LcdConsole.WriteLine(connectionURL);

            RestClient restClient = new RestClient(connectionURL);

            var request = new RestRequest(Method.GET);
            var response = restClient.Execute(request);

            LcdConsole.WriteLine("StatusCode: " + response.StatusCode);
            if (response.ErrorMessage != null)
            {
                LcdConsole.WriteLine(response.ErrorMessage);
            }
            if (response.StatusCode == HttpStatusCode.OK)
            {
                LcdConsole.WriteLine("Content Length: " + response.Content.Length);
            }
            if (response.Content != null)
            {
                LcdConsole.WriteLine("Content: " + response.Content.Substring(0, response.Content.Length < 30 ? response.Content.Length : 30));
            }

            return MonoBrickDebugHelper.MessageYesNo("Press Enter to continue or Escape to exit");
        }

        private static Config LoadConfig(string path)
        {
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path, Encoding.UTF8);
                string json = sr.ReadToEnd();
                return JsonConvert.DeserializeObject<Config>(json);
            }
            return null;
        }

        public static void Main (string[] args)
		{
            config = LoadConfig(ConfigFilePath);

            ipfsClient = new IPFSClient(string.Format("{0}:{1}", config.IPFSConfig.Host, config.IPFSConfig.Port));
            rpcClient = new LegacyRpcClient(new Uri(string.Format("{0}:{1}", config.EthereumConfig.Host, config.EthereumConfig.Port)));

            //BigInteger[] tokenIds = GetItems();

            byte[] addressBytes = Base58Utils.Base58CheckEncoding.DecodePlain("QmQzCQn4puG4qu8PVysxZmscmQ5vT1ZXpqo7f58Uh9QfyY");
            byte[] dataBytes = null;
            dataBytes = ipfsClient.DownloadBytes(addressBytes);
            string dataString = Encoding.ASCII.GetString(dataBytes);

            TestSvgParsing();
            LcdConsole.WriteLine("Starting...");
            InitializeBrick();

            ShowMainMenu();  
        }

        private static void ShowMainMenu()
        {
            MenuContainer container;
            Menu menu = new Menu("Main menu");
            menu.AddItem(new CalibrationView());
            menu.AddItem(new BlockStormBlockchainControl());

            container = new MenuContainer(menu);
            container.Show(true);
        }

        private static void InitializeBrick()
        {
            MotorSetup setup = new MotorSetup();
            setup.VMotor = new Motor(MotorPort.OutA);
            setup.VMotor.ResetTacho();
            setup.LeftMotor = new Motor(MotorPort.OutC);
            setup.RightMotor = new Motor(MotorPort.OutD);

            BrickSettings.MotorSetup = setup;
        }

        private static void TestSvgParsing()
        {
            string svgString =
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" +
"<!-- Created with Inkscape (http://www.inkscape.org/) -->" +
"" +
"<svg" +
"   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"" +
"   xmlns:cc=\"http://creativecommons.org/ns#\"" +
"   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" +
"   xmlns:svg=\"http://www.w3.org/2000/svg\"" +
"   xmlns=\"http://www.w3.org/2000/svg\"" +
"   xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"" +
"   xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"" +
"   version=\"1.1\"" +
"   id=\"svg5097\"" +
"   width=\"800\"" +
"   height=\"800\"" +
"   viewBox=\"0 0 800 800\"" +
"   sodipodi:docname=\"ethereum-ETHBerlin.svg\"" +
"   inkscape:version=\"0.92.3 (2405546, 2018-03-11)\">" +
"  <metadata" +
"     id=\"metadata5103\">" +
"    <rdf:RDF>" +
"      <cc:Work" +
"         rdf:about=\"\">" +
"        <dc:format>image/svg+xml</dc:format>" +
"        <dc:type" +
"           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />" +
"        <dc:title></dc:title>" +
"      </cc:Work>" +
"    </rdf:RDF>" +
"  </metadata>" +
"  <defs" +
"     id=\"defs5101\" />" +
"  <sodipodi:namedview" +
"     pagecolor=\"#ffffff\"" +
"     bordercolor=\"#666666\"" +
"     borderopacity=\"1\"" +
"     objecttolerance=\"10\"" +
"     gridtolerance=\"10\"" +
"     guidetolerance=\"10\"" +
"     inkscape:pageopacity=\"0\"" +
"     inkscape:pageshadow=\"2\"" +
"     inkscape:window-width=\"2560\"" +
"     inkscape:window-height=\"1346\"" +
"     id=\"namedview5099\"" +
"     showgrid=\"false\"" +
"     inkscape:zoom=\"1.40375\"" +
"     inkscape:cx=\"400\"" +
"     inkscape:cy=\"400\"" +
"     inkscape:window-x=\"-11\"" +
"     inkscape:window-y=\"-11\"" +
"     inkscape:window-maximized=\"1\"" +
"     inkscape:current-layer=\"svg5097\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 401.06857,49.154052 602.67142,381.83437\"" +
"     id=\"path5121\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 604.09617,413.89136 401.78094,701.6919 195.90383,414.60374\"" +
"     id=\"path5123\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 199.46572,380.40962 401.06857,49.154052 399.64381,498.66429\"" +
"     id=\"path5125\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 401.78094,701.6919 400.35619,535.70793 604.09617,413.89136\"" +
"     id=\"path5127\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 602.67142,381.83437 399.64381,498.66429 199.46572,380.40962\"" +
"     id=\"path5129\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"M 195.90383,414.60374 400.35619,535.70793\"" +
"     id=\"path5131\"" +
"     inkscape:connector-curvature=\"0\" />" +
"  <path" +
"     style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1\"" +
"     d=\"m 199.46572,380.40962 200.17809,-90.47195 203.02761,91.8967\"" +
"     id=\"path5133\"" +
"     inkscape:connector-curvature=\"0\" />" +
"</svg>";
            LcdConsole.WriteLine("Parsing....");
            XElement svg = XElement.Parse(svgString);
            var first = svg.FirstNode;
            var second = first.NextNode;
            XName pName = XName.Get("path", @"http://www.w3.org/2000/svg");
            int foundN = 0;
            //LEGOLogoDriver driver = new LEGOLogoDriver();
            foreach(var elem in svg.Elements(pName))
            {
                SVGConverter.SvgPathBuilder.Parse(elem.Attribute("d").Value.ToString());
                ++foundN;
            }
            LcdConsole.WriteLine("Found {0} elements.", foundN);
        }

        private static void MoveUpDown()
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += (source, e) =>
            {
                //motorC.Brake();
               // motorD.Brake();
                timer.Stop();
            };
            timer.Stop();
            using (ButtonEvents buts = new ButtonEvents())
            {
                buts.EscapePressed += () => {
                    escapePressed = true;
                    LcdConsole.WriteLine("EscapePressed");
                };
                buts.UpPressed += () =>
                {
                    //var motorWaitHandle = motorA.SpeedProfile(50, 100, 300, 100, true);
                    //LcdConsole.WriteLine("Waiting for motor A to stop");
                    //motorWaitHandle.WaitOne();
                    //LcdConsole.WriteLine("Done moving motor");
                    //LcdConsole.WriteLine("Position A: " + motorA.GetTachoCount());
                };
                buts.UpReleased += () =>
                {
                    //motorA.Brake();
                };
                buts.DownPressed += () =>
                {
                    //motorA.SetSpeed(-30);
                };
                buts.DownReleased += () =>
                {
                    //motorA.Brake();
                    //LcdConsole.WriteLine("Position A: " + motorA.GetTachoCount());
                };
                buts.EnterReleased += () =>
                {
                    //motorC.SetSpeed(30);
                    //motorD.SetSpeed(30);
                    timer.Start();
                };

                while (true)
                {
                    if (escapePressed)
                    {
                        //motorA.Off();
                        //motorC.Off();
                        //motorC.Off();
                        LcdConsole.WriteLine("Exit");
                        break;
                    }

                    Thread.Sleep(1000);
                }
            }
        }

        static void AvoidObstacle()
        {
            bool setSpeed = true;

            //distance = sensor.ReadDistance();
            while (distance <= stop_distance)
            {
                LcdConsole.WriteLine("Distance: " + distance);
                LcdConsole.WriteLine("Turn");

                int direction = random.Next(0, 2);
                if (direction == 0)
                    direction = -1;
                    
                if (setSpeed)
                {
                   // motorA.SetSpeed((sbyte)(50 * direction));
                    //motorD.SetSpeed((sbyte)(50 * direction * (-1)));
                    setSpeed = false;
                }

                Thread.Sleep(1500);

                if (escapePressed)
                    break;

                //distance = sensor.ReadDistance();
            }

            //motorA.Brake();
           // motorD.Brake();
        }

        static void Run()
        {
            //motorA.SetSpeed(50);
            //motorD.SetSpeed(50);

            while (distance > stop_distance)
            {
                LcdConsole.WriteLine("Distance: " + distance);
                Thread.Sleep(100);
                //distance = sensor.ReadDistance();

                if (escapePressed)
                    return;
            }

            //motorA.Brake();
            //motorD.Brake();
        }
    }
}
