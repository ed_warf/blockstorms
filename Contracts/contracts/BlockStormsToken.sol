pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";

contract BlockStormsToken is ERC721Token {

    using SafeMath for uint256;

    mapping(uint256 => bytes32) private _tokenNames;
    
    constructor(string _name, string _symbol) ERC721Token(_name, _symbol) public {
    }
    
    function mint(bytes32 _tokenName, uint256 _tokenId) public returns (bool){
        require(!exists(_tokenId));
        _mint(msg.sender, _tokenId);
        _tokenNames[_tokenId] = _tokenName;
    }
    
    function tokenName(uint256 _tokenId) public view returns (bytes32) {
        return _tokenNames[_tokenId];
    }
}